package com.bean.gmailservice.service;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;

@Service
public interface GMailApiService {

    String authorizeRequest() throws GeneralSecurityException, IOException;

    String fetchSearchQueryEmailResponse(String code) throws IOException, GeneralSecurityException;
}
