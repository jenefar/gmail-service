package com.bean.gmailservice.service;

import com.google.api.client.auth.oauth2.AuthorizationCodeRequestUrl;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets.Details;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePart;
import com.google.api.services.gmail.model.MessagePartBody;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.*;

/**
 * @author jenefar
 */
@Service
public class GMailApiServiceImpl implements GMailApiService {

    @Autowired
    Environment env;

    private static HttpTransport httpTransport;
    private static final String APPLICATION_NAME = "Bean";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();


    GoogleClientSecrets clientSecrets;
    GoogleAuthorizationCodeFlow flow;
    Credential credential;

    @Override
    public String authorizeRequest() throws GeneralSecurityException, IOException {

        if (flow == null) {
            Details web = new Details();
            web.setClientId(env.getProperty("gmail.client.clientId"));
            web.setClientSecret(env.getProperty("gmail.client.clientSecret"));
            clientSecrets = new GoogleClientSecrets().setWeb(web);
            httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            flow = new GoogleAuthorizationCodeFlow.Builder(httpTransport, JacksonFactory.getDefaultInstance(), clientSecrets,
                    Collections.singleton(GmailScopes.GMAIL_READONLY)).build();

        }

        AuthorizationCodeRequestUrl authorizationUrl = flow.newAuthorizationUrl().setRedirectUri(env.getProperty("gmail.client.redirectUri"));
        System.out.println("authorization url:" + authorizationUrl);
        return authorizationUrl.build();
    }

    @Override
    public String fetchSearchQueryEmailResponse(String code) throws IOException, GeneralSecurityException {

        JSONObject json = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

        TokenResponse response = flow.newTokenRequest(code).setRedirectUri(env.getProperty("gmail.client.redirectUri")).execute();
        credential = flow.createAndStoreCredential(response, "userID");


        Gmail service = new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();

// here solr is the keyword to be searched for in the attachments
        String user = "me";
        String query = "solr has:attachment";

        List<Message> messages = listMessagesMatchingQuery(service, user, query);

        System.out.println("message count total:" + messages.size());

        jsonArray.put(messages.toArray());
        json.put("response", jsonArray);
        return json.toString();
    }


    public  List<Message> listMessagesMatchingQuery(Gmail service, String userId,
                                                          String query) throws IOException {
        ListMessagesResponse response = service.users().messages().list(userId).setQ(query).execute();

        System.out.println("Message count total:" + service.users().getProfile(userId).execute().getMessagesTotal());
        ;

        System.out.println("Result set count:" + service.users().messages().list(userId).setQ(query).execute());

        List<Message> messages = new ArrayList<Message>();
        while (response.getMessages() != null) {
            messages.addAll(response.getMessages());
            System.out.println("message size for each page:" + response.getResultSizeEstimate());
            for (Message msg : response.getMessages()) {
                getAttachments(service, userId, msg.getId());
            }
            if (response.getNextPageToken() != null) {
                String pageToken = response.getNextPageToken();
                response = service.users().messages().list(userId).setQ(query)
                        .setPageToken(pageToken).execute();


            } else {
                break;
            }
        }

        return messages;
    }

    public  void getAttachments(Gmail service, String userId, String messageId)
            throws IOException {
        Message message = service.users().messages().get(userId, messageId).execute();
        System.out.println("Message is:" + message.toString());
        List<MessagePart> parts = message.getPayload().getParts();
        for (MessagePart part : parts) {
            if (part.getFilename() != null && part.getFilename().length() > 0) {
                String filename = part.getFilename();
                Optional<String> fileExtension= getExtensionByStringHandling(filename);
                System.out.println("file extension:::::::" + fileExtension.get());

                List<String> whiteListFileExtensions= new ArrayList<>();
                whiteListFileExtensions.add("doc");
                whiteListFileExtensions.add("docx");
                whiteListFileExtensions.add("pdf");
                if(whiteListFileExtensions.contains(fileExtension.get())) {
                    String attId = part.getBody().getAttachmentId();
                    MessagePartBody attachPart = service.users().messages().attachments().
                            get(userId, messageId, attId).execute();

                    byte[] fileByteArray = Base64.getUrlDecoder().decode(attachPart.getData());
                    FileOutputStream fileOutFile =
                            new FileOutputStream("/Users/jenefar/gmail-attachments/" + filename);
                    fileOutFile.write(fileByteArray);
                    fileOutFile.close();
                }
            }
        }
    }

    public Optional<String> getExtensionByStringHandling(String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }


}










    


