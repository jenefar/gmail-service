package com.bean.gmailservice.controller;

import com.bean.gmailservice.service.GMailApiServiceImpl;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * @author jenefar
 */
@RestController
public class GoogleMailController {

    @Autowired
    Environment env;

    @Autowired
    GMailApiServiceImpl gMailApiService;


    @RequestMapping(value = "/login/gmail", method = RequestMethod.GET)
    public RedirectView gmailLogin(HttpServletRequest request) throws GeneralSecurityException, IOException {
        return new RedirectView(gMailApiService.authorizeRequest());

    }

    @RequestMapping(value = "/gmail/callback", method =RequestMethod.GET , params = "code")
    public ResponseEntity<String> oAuth2Callback(@RequestParam(value = "code") String code) throws IOException, GeneralSecurityException {

        String jsonResponse=gMailApiService.fetchSearchQueryEmailResponse(code);
        return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
    }
}
